var express = require('express');
var favicon = require('serve-favicon');
var app = express();

app.get('/', function(request, response) {
	response.sendFile(__dirname + '/static_content/index.html');
	console.log('Llamada recibida a la ruta: "/"');
});

// Contenido estático.
app.use(favicon(__dirname + '/static_content/favicon.ico'));
app.use('/content', express.static(__dirname + '/static_content'));

app.get('/:controller/:action', function(request, response) {
	response.send('Controlador: ' + request.params.controller + "<br>Acción: " + request.params.action);
	console.log('Llamada recibida a la ruta: "/' + request.params.controller + '/' + request.params.action + '"');
});

app.listen(3000);

console.log('Escuchando en el puerto 3000');